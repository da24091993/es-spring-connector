package com.generic.elastic.esspringconnector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsSpringConnectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsSpringConnectorApplication.class, args);
	}

}
