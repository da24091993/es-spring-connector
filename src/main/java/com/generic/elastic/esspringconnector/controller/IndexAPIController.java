package com.generic.elastic.esspringconnector.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.generic.elastic.esspringconnector.model.ElasticSearchIndexResponse;
import com.generic.elastic.esspringconnector.service.IndexAPIService;

@RestController
@RequestMapping("/")
public class IndexAPIController {
	
	@Autowired
	private IndexAPIService indexAPIService;
	
	
	@GetMapping("/index")
	public ElasticSearchIndexResponse getData(@RequestParam("index") String index) throws IOException {
		return indexAPIService.index(index);
	}

}
