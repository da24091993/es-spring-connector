package com.generic.elastic.esspringconnector.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.generic.elastic.esspringconnector.model.ElasticSearchGetResponse;
import com.generic.elastic.esspringconnector.service.GetAPIService;

@RestController
@RequestMapping("/")
public class GetAPIController {

	@Autowired
	private GetAPIService getAPIService;
	
	
	@GetMapping("/data")
	public ElasticSearchGetResponse getData(@RequestParam("index") String index, @RequestParam("docID") String docID) throws IOException {
		return getAPIService.getData(index, docID);
	}
	
}
