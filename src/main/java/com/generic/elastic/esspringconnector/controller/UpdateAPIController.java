package com.generic.elastic.esspringconnector.controller;

import java.io.IOException;

import org.elasticsearch.action.update.UpdateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.generic.elastic.esspringconnector.model.ElasticSearchUpdateResponse;
import com.generic.elastic.esspringconnector.service.UpdateAPIService;

@RestController
@RequestMapping("/")
public class UpdateAPIController {
	
	@Autowired
	private UpdateAPIService UpdateAPIService;
	
	
	@GetMapping("/update")
	public ElasticSearchUpdateResponse updateData(@RequestParam("index") String index, @RequestParam("docID") String docID) throws IOException {
		return UpdateAPIService.updateData(index,docID);
	}

}
