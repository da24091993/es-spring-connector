package com.generic.elastic.esspringconnector.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.generic.elastic.esspringconnector.model.ElasticSearchTermVectorResponse;
import com.generic.elastic.esspringconnector.service.TermVectorAPIService;


@RestController
@RequestMapping("/")
public class TermVectorAPIController {
	
	@Autowired
	private TermVectorAPIService TVAPIService;
	
	
	@GetMapping("/termvector")
	public ElasticSearchTermVectorResponse getInfo(@RequestParam("index") String index,@RequestParam("docID") String docID) throws IOException {
		return TVAPIService.getInfo(index,docID);
	}

}
