package com.generic.elastic.esspringconnector.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.generic.elastic.esspringconnector.model.ElasticSearchDeleteResponse;
import com.generic.elastic.esspringconnector.service.DeleteAPIService;
@RestController
@RequestMapping("/")
public class DeleteAPIController {
	
	@Autowired
	private DeleteAPIService DeleteAPIService;
	
	
	@GetMapping("/delete")
	public ElasticSearchDeleteResponse removeData(@RequestParam("index") String index, @RequestParam("docID") String docID) throws IOException {
		return DeleteAPIService.removeData(index,docID);
	}

}
