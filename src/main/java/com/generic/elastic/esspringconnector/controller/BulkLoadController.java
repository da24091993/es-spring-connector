package com.generic.elastic.esspringconnector.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.generic.elastic.esspringconnector.model.ElasticSearchLoadResponse;
import com.generic.elastic.esspringconnector.service.BulkLoadService;

@RestController
@RequestMapping("/")
public class BulkLoadController {

	@Autowired
	private BulkLoadService bulkLoadService;
	
	
	@GetMapping("/BulkUpload")
	public ElasticSearchLoadResponse bulkLoad(@RequestParam("index") String index, @RequestParam("docID") String docID) throws IOException {
		return bulkLoadService.bulkLoad(index , docID);
	}
}

