package com.generic.elastic.esspringconnector.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.generic.elastic.esspringconnector.model.ElasticSearchExistResponse;
import com.generic.elastic.esspringconnector.model.ElasticSearchIndexResponse;
import com.generic.elastic.esspringconnector.service.ExistAPIService;

@RestController
@RequestMapping("/")
public class ExistAPIController {
	
	@Autowired
	private ExistAPIService ExistAPIService;
	
	
	@GetMapping("/exist")
	public ElasticSearchExistResponse getData(@RequestParam("index") String index, @RequestParam("docID") String docID) throws IOException {
		return ExistAPIService.getData(index,docID);
	}

}
