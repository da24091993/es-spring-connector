package com.generic.elastic.esspringconnector.service;

import java.io.IOException;

import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.generic.elastic.esspringconnector.model.ElasticSearchGetResponse;

@Service
public class GetAPIService {
	
	@Autowired
	private RestHighLevelClient client;
	
	public ElasticSearchGetResponse getData(String index, String docID) throws IOException {
		
		ElasticSearchGetResponse response = new ElasticSearchGetResponse();
		GetRequest request = new GetRequest(index, docID);
		GetResponse responseElasticSearch = client.get(request, RequestOptions.DEFAULT);
		response.setDocID(responseElasticSearch.getId());
		response.setSource(responseElasticSearch.getSourceAsMap());
		return response;
	}

}
