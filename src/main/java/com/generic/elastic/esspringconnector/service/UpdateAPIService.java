package com.generic.elastic.esspringconnector.service;

import java.io.IOException;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.generic.elastic.esspringconnector.model.ElasticSearchUpdateResponse;


@Service
public class UpdateAPIService {
	
	@Autowired
	private RestHighLevelClient client;
	
	public ElasticSearchUpdateResponse updateData(String index, String docID) throws IOException
	{
		ElasticSearchUpdateResponse response = new ElasticSearchUpdateResponse(); 
		UpdateRequest request = new UpdateRequest(index,  docID); 
		String jsonString = "{" +
		        "\"updated\":\"2017-01-01\"," +
		        "\"reason\":\"daily update\"" +
		        "}";
		request.doc(jsonString, XContentType.JSON);
		UpdateResponse updateResponse=null;
		try {
			updateResponse = client.update(
			        request, RequestOptions.DEFAULT);
		} catch (ElasticsearchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.setIndex(updateResponse.getIndex());
		response.setId(updateResponse.getId());
		response.setJson(updateResponse.getResult());
		return response;
	}

	

}
