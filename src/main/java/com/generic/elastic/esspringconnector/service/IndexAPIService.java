package com.generic.elastic.esspringconnector.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.generic.elastic.esspringconnector.model.ElasticSearchIndexResponse;

@Service
public class IndexAPIService {
	
	@Autowired
	private RestHighLevelClient client;
	
	public ElasticSearchIndexResponse index(String index) throws IOException {

		ElasticSearchIndexResponse response = new ElasticSearchIndexResponse();
		Map<String, Object> jsonMap = new HashMap<>();
		jsonMap.put("title", "Harry Potter - Chamber of Secrets");
		jsonMap.put("year", "2004");
		IndexRequest indexRequest = new IndexRequest(index)
				.source(jsonMap);
		IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
		response.setIndex(indexResponse.getIndex());
		response.setId(indexResponse.getId());
		if (indexResponse.getResult() == DocWriteResponse.Result.CREATED) {
		    response.setResult(DocWriteResponse.Result.CREATED.name());
		} else if (indexResponse.getResult() == DocWriteResponse.Result.UPDATED) {
			 response.setResult(DocWriteResponse.Result.UPDATED.name());
		}
		else {
			response.setResult(indexResponse.getResult().name());
		}
		return response;
	}

}
