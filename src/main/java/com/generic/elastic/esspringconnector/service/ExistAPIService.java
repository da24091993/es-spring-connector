package com.generic.elastic.esspringconnector.service;

import java.io.IOException;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.generic.elastic.esspringconnector.model.ElasticSearchExistResponse;
import com.generic.elastic.esspringconnector.model.ElasticSearchGetResponse;

@Service
public class ExistAPIService {
	
	@Autowired
	private RestHighLevelClient client;
	
	public ElasticSearchExistResponse getData(String index, String docID) throws IOException {
		
		ElasticSearchExistResponse response = new ElasticSearchExistResponse();
		GetRequest getRequest = new GetRequest(index,docID);    
			getRequest.fetchSourceContext(new FetchSourceContext(false)); 
			getRequest.storedFields("_none_"); 
			
			boolean exists = client.exists(getRequest, RequestOptions.DEFAULT);
			GetRequest request = new GetRequest(index, docID);
			GetResponse responseElasticSearch = client.get(request, RequestOptions.DEFAULT);

			ActionListener<Boolean> listener = new ActionListener<Boolean>() {
			    @Override
			    public void onResponse(Boolean exists) 
			    {
			    	   if(exists)
			        	response.setField("true");
			    	   else

				        	response.setField("false");  
			        	response.setDocID(responseElasticSearch.getId());
			        	response.setSource(responseElasticSearch.getSourceAsMap());
			        		    }

			    @Override
			    public void onFailure(Exception e)
			    {
			      e.getStackTrace();    
			    }
			    
			};
			
		listener.onResponse(exists);
		return response;
	}

	

}
