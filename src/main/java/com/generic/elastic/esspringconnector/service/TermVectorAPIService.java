package com.generic.elastic.esspringconnector.service;

import java.io.IOException;

import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.TermVectorsRequest;
import org.elasticsearch.client.core.TermVectorsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.generic.elastic.esspringconnector.model.ElasticSearchDeleteResponse;
import com.generic.elastic.esspringconnector.model.ElasticSearchTermVectorResponse;


@Service
public class TermVectorAPIService {
	

@Autowired
private RestHighLevelClient client;

	public ElasticSearchTermVectorResponse getInfo(String index, String docID) throws IOException {
		// TODO Auto-generated method stub
		
		ElasticSearchTermVectorResponse response = new ElasticSearchTermVectorResponse();
		TermVectorsRequest request = new TermVectorsRequest(index, docID);
		request.setFields("user");
		
		TermVectorsResponse res =
		        client.termvectors(request, RequestOptions.DEFAULT);
		
		response.setIndex(res.getIndex());
		response.setType(res.getType());
		response.setId(res.getId());
		response.setFound(res.getFound());
		
		
		return response;
	}

}
