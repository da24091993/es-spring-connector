package com.generic.elastic.esspringconnector.service;

import java.io.IOException;

import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.support.replication.ReplicationResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.generic.elastic.esspringconnector.model.ElasticSearchDeleteResponse;
@Service
public class DeleteAPIService {
	
	@Autowired
	private RestHighLevelClient client;

	public ElasticSearchDeleteResponse removeData(String index,String docID) throws IOException {
		// TODO Auto-generated method stub
		
		ElasticSearchDeleteResponse response = new ElasticSearchDeleteResponse();

		DeleteRequest request = new DeleteRequest(index,docID);  
		DeleteResponse DeleteResponse = client.delete(
		        request, RequestOptions.DEFAULT);

		response.setIndex(DeleteResponse.getIndex());
		response.setId(DeleteResponse.getId());
		response.setVersion(DeleteResponse.getVersion());
		
		ReplicationResponse.ShardInfo shardInfo = DeleteResponse.getShardInfo();
		if (shardInfo.getTotal() != shardInfo.getSuccessful()) {
		    
		}
		if (shardInfo.getFailed() > 0) {
		    for (ReplicationResponse.ShardInfo.Failure failure :
		            shardInfo.getFailures()) {
		        String reason = failure.reason(); 
		        response.setReason(reason);
		    }
		}
		return response;
	}

}
