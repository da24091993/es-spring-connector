package com.generic.elastic.esspringconnector.service;

import java.io.IOException;

import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.generic.elastic.esspringconnector.model.ElasticSearchLoadResponse;

@Service
public class BulkLoadService {
	
	@Autowired
	private RestHighLevelClient client;
	
	public ElasticSearchLoadResponse bulkLoad(String index, String docID) throws IOException {
		
		ElasticSearchLoadResponse response = new ElasticSearchLoadResponse();

		BulkRequest request = new BulkRequest(); 
		request.add(new IndexRequest(index).id(docID)  
		        .source(XContentType.JSON,"field", "foo"));
		
	BulkResponse bulkResponse = client.bulk(request, RequestOptions.DEFAULT);
	
	for (BulkItemResponse bulkItemResponse : bulkResponse) { 
	    DocWriteResponse itemResponse = bulkItemResponse.getResponse(); 

	    switch (bulkItemResponse.getOpType()) {
	    case INDEX:    
	    case CREATE:
	        IndexResponse indexResponse = (IndexResponse) itemResponse;
	        response.setIndex(indexResponse.getIndex());
	        response.setID(indexResponse.getId());
	        break;
	    case UPDATE:   
	        UpdateResponse updateResponse = (UpdateResponse) itemResponse;
	        response.setID(updateResponse.getId());
	        response.setIndex(updateResponse.getIndex());
	        response.setResult(updateResponse.getGetResult());
	        break;
	    case DELETE:   
	        DeleteResponse deleteResponse = (DeleteResponse) itemResponse;
	        response.setID(deleteResponse.getId());
	        response.setIndex(deleteResponse.getIndex());
	        response.setResult(deleteResponse.getResult());
	        
	    }
	}
	return response;
	}
}