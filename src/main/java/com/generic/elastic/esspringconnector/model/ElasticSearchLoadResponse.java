package com.generic.elastic.esspringconnector.model;

import org.elasticsearch.action.DocWriteResponse.Result;
import org.elasticsearch.index.get.GetResult;

public class ElasticSearchLoadResponse {
	String Index;
	String ID;
	GetResult Result;
	Result SResult;
	public void setIndex(String index) {
		// TODO Auto-generated method stub
		Index = index;
	}
	
	public String getIndex() {
		return Index;
	}
	
	public void setID(String id) {
		// TODO Auto-generated method stub
		ID = id;
	}
	
	public String getID() {
		return ID;
	}

	public void setResult(GetResult getResult) {
		// TODO Auto-generated method stub
		Result = getResult;
	}
	
	public GetResult getResult() {
		return Result;
	}

	public void setResult(Result result2) {
		// TODO Auto-generated method stub
		SResult = result2;
	}
	
	public Result result2() {
		return SResult;
	}
}
