package com.generic.elastic.esspringconnector.model;

public class ElasticSearchIndexResponse {
	
	private String Index;
	
	private String Id;
	
	private String Result;

	public String getIndex() {
		return Index;
	}

	public void setIndex(String index) {
		this.Index = index;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getResult() {
		return Result;
	}

	public void setResult(String result) {
		Result = result;
	}
	
	

}
