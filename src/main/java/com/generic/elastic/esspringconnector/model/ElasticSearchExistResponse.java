package com.generic.elastic.esspringconnector.model;

import java.util.Map;

public class ElasticSearchExistResponse {

	
	private String DocID;
	
	private Map<String, Object> Source;
	
	private String Found;
	

	public String getDocID() {
		return DocID;
	}

	public void setDocID(String docID) {
		this.DocID = docID;
	}

	public Map<String, Object> getSource() {
		return Source;
	}

	public void setSource(Map<String, Object> source) {
		this.Source = source;
	}
	
	public void setField(String f) {
		Found = f;
	}
	
	public String getField() {
		return Found;
	}
	
	
	
	
	
}
