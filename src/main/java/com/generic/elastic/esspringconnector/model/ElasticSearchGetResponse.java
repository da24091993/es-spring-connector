package com.generic.elastic.esspringconnector.model;

import java.util.Map;

public class ElasticSearchGetResponse {

	
	private String DocID;
	
	private Map<String, Object> Source;

	public String getDocID() {
		return DocID;
	}

	public void setDocID(String docID) {
		DocID = docID;
	}

	public Map<String, Object> getSource() {
		return Source;
	}

	public void setSource(Map<String, Object> source) {
		Source = source;
	}
	
	
	
}
